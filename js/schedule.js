var date = document.getElementById("date");
var sched = document.getElementById("sched");

date.value = new Date().toJSON().slice(0,10);

var url="../content/sched_personal.php";

function getajax(){
    var xhr = new XMLHttpRequest();

    xhr.onload = function() {
        if (xhr.status === 200) {
            process_data(xhr.responseText);
        }

        else {
            console.log(xhr.responseText);
        }

    };

    xhr.open("GET", url, true);
    xhr.send();

}

getajax();

function compare(obj1, obj2) {
    if (obj1["day"] < obj2["day"]) {
        return -1;
    }

    if (obj1["day"] > obj2["day"]) {
        return 1;
    }

    if (obj1["day"] == obj2["day"]) {
        if (obj1["beginning"] < obj2["beginning"]) {
            return -1;
        }

        if (obj1["beginning"] > obj2["beginning"]) {
            return 1;
        }

        return 0;
    }
}

function resetSched() {

    var row = "<tr><td>Ден</td>";

    for (var i = 8; i < 21; i++) {
        row += "<td>"+i+"</td>";
    }

    row += "</tr>";

    sched.innerHTML = row;
}

function toDay(day) {
    switch(day) {
        case 0:
            return "Понеделник";
        case 1:
            return "Вторник";
        case 2:
            return "Сряда";
        case 3:
            return "Четвъртък";
        case 4:
            return "Петък";
        case 5:
            return "Събота";
        case 6:
            return "Неделя";
    }
}

function process_data(json) {
    var subjects = JSON.parse(json);
    var current = 0;

    subjects.sort(compare);
    resetSched();

    for (var i = 0; i < 7; i++) {
        var row = "<tr><td>"+toDay(i)+"</td>";

        for (var j = 8; j < 21; j++) {
            if (current >= subjects.length || subjects[current]["day"] > i || subjects[current]["beginning"] > j ) {
                row += "<td></td>";
            }

            else {
                s = subjects[current++];
                row += "<td colspan="+s["duration"] + ">"+ s["name"] + ", " + s["degree"] + ", " + s["year"] +", " + (s["subgroup"] > 0?s["subgroup"] + ", ":"")  + s["teacher"] + ", " + s["room"] + ", " + s["building"] + "</td>";
                j+=s["duration"]-1;
            }
        }

        row += "</tr>";
        sched.innerHTML+=row;
    }
}
