var date_name = document.getElementById("date_name");
var sched_name = document.getElementById("sched_name");


var url_name="../content/sched_name.php";
var xhr;
function postajax_name(){
    var xhr = new XMLHttpRequest();

    xhr.onload = function() {
        if (xhr.status === 200) {
            process_data_name(xhr.responseText);
        }

        else {
            console.log(xhr.response);
        }
    };

    xhr.open("POST", url_name, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    params = "usr=" + date_name.value;
    console.log(params);
    xhr.send(params);

}


function compare_name(obj1, obj2) {
    if (obj1["day"] < obj2["day"]) {
        return -1;
    }

    if (obj1["day"] > obj2["day"]) {
        return 1;
    }

    if (obj1["day"] == obj2["day"]) {
        if (obj1["beginning"] < obj2["beginning"]) {
            return -1;
        }

        if (obj1["beginning"] > obj2["beginning"]) {
            return 1;
        }

        return 0;
    }
}

function resetSched_name() {

    var row = "<tr><td>Ден</td>";

    for (var i = 8; i < 21; i++) {
        row += "<td>"+i+"</td>";
    }

    row += "</tr>";

    sched_name.innerHTML = row;
}

function toDay_name(day) {
    switch(day) {
        case 0:
            return "Понеделник";
        case 1:
            return "Вторник";
        case 2:
            return "Сряда";
        case 3:
            return "Четвъртък";
        case 4:
            return "Петък";
        case 5:
            return "Събота";
        case 6:
            return "Неделя";
    }
}

function process_data_name(json) {
    var subjects = JSON.parse(json);
    var current = 0;

    subjects.sort(compare_name);
    resetSched_name();

    for (var i = 0; i < 7; i++) {
        var row = "<tr><td>"+toDay_name(i)+"</td>";

        for (var j = 8; j < 21; j++) {
            if (current >= subjects.length || subjects[current]["day"] > i || subjects[current]["beginning"] > j ) {
                row += "<td></td>";
            }

            else {
                s = subjects[current++];
                row += "<td colspan="+s["duration"] + ">"+ s["name"] + ", " + s["degree"]+ ", " + s["year"] + ", " + (s["subgroup"] >0?s["subgroup"] + ", ":"") + s["teacher"] + ", " + s["room"] + ", " + s["building"] + "</td>";
                j+=s["duration"]-1;
            }
        }

        row += "</tr>";
        sched_name.innerHTML+=row;
    }

}



function drawinit_r() {
    resetSched_name();
    for (var i = 0; i < 7; i++) {
        var row = "<tr><td>"+toDay_name(i)+"</td>";

        for (var j = 8; j < 21; j++) {
                row += "<td></td>";


        }

        row += "</tr>";
        sched_name.innerHTML+=row;
    }
}

drawinit_r();
