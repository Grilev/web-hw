var date_room = document.getElementById("date_room");
var sched_room = document.getElementById("sched_room");


var url_room="../content/sched_room.php";
var xhr;
function postajax_room(){
    var xhr = new XMLHttpRequest();

    xhr.onload = function() {
        if (xhr.status === 200) {
            console.log(xhr.response);
            console.log(xhr);
            process_data_room(xhr.responseText);
        }

        else {
            console.log(xhr.response);
        }
    };

    xhr.open("POST", url_room, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    params = "usr=" + date_room.value;
    console.log(params);
    xhr.send(params);

}


function compare_room(obj1, obj2) {
    if (obj1["day"] < obj2["day"]) {
        return -1;
    }

    if (obj1["day"] > obj2["day"]) {
        return 1;
    }

    if (obj1["day"] == obj2["day"]) {
        if (obj1["beginning"] < obj2["beginning"]) {
            return -1;
        }

        if (obj1["beginning"] > obj2["beginning"]) {
            return 1;
        }

        return 0;
    }
}

function resetSched_room() {

    var row = "<tr><td>Ден</td>";

    for (var i = 8; i < 21; i++) {
        row += "<td class>"+i+"</td>";
    }

    row += "</tr>";

    sched_room.innerHTML = row;
}

function toDay_room(day) {
    switch(day) {
        case 0:
            return "Понеделник";
        case 1:
            return "Вторник";
        case 2:
            return "Сряда";
        case 3:
            return "Четвъртък";
        case 4:
            return "Петък";
        case 5:
            return "Събота";
        case 6:
            return "Неделя";
    }
}

function process_data_room(json) {
    var subjects = JSON.parse(json);
    var current = 0;

    subjects.sort(compare_room);
    console.log(subjects);
    resetSched_room();

    for (var i = 0; i < 7; i++) {
        var row = "<tr><td>"+toDay_room(i)+"</td>";

        for (var j = 8; j < 21; j++) {
            if (current >= subjects.length || subjects[current]["day"] > i || subjects[current]["beginning"] > j ) {
                row += "<td></td>";
            }

            else {
                s = subjects[current++];
                row += "<td colspan="+s["duration"] + ">"+ s["name"] + ", " + s["degree"]+ ", " + s["year"] + ", " + (s["subgroup"] > 0?s["subgroup"] + ", ":"") + s["teacher"] + ", " + s["room"] + ", " + s["building"] + "</td>";
                j+=s["duration"]-1;
            }
        }

        row += "</tr>";
        sched_room.innerHTML+=row;
    }

}

function drawinit() {
    resetSched_room();
    for (var i = 0; i < 7; i++) {
        var row = "<tr><td>"+toDay_room(i)+"</td>";

        for (var j = 8; j < 21; j++) {
                row += "<td></td>";


        }

        row += "</tr>";
        sched_room.innerHTML+=row;
    }
}

drawinit();
