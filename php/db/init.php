<?php
    global $config;
    $config = include("../config.php");

    global $connection;
    $connection = new PDO(
        $config["db_engine"] .
            ":" .
            $config["host"] .
            ";dbname=" .
            $config["db_name"] .
            ";charset=" .
            $config["charset"],
        $config["db_user"],
        $config["db_pass"]
    );

 ?>
