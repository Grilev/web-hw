<?php
    include("init.php");


    function getUserInfo($username) {
        global $connection;global $config;

        $statement = $connection -> prepare("select users.username, roles.name as role, users.name, users.year, users.subgroup, users.degree
                                            from " . $config["db_name"] . ".users
                                            inner join " . $config["db_name"] . ".roles on users.role_id=roles.id
                                            where users.username = ?");

        $statement -> execute(array($username));

        $result = $statement -> fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    function getUserInfoName($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("select users.username, roles.name as role, users.name, users.year, users.subgroup, users.degree
                                            from " . $config["db_name"] . ".users
                                            inner join " . $config["db_name"] . ".roles on users.role_id=roles.id
                                            where users.name = ?");

        $statement -> execute(array($name));

        $result = $statement -> fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    function getRole($username) {
        return @getUserInfo($username)["role"];
    }

    function getRoles() {
        global $connection;global $config;

        $statement = $connection -> prepare("select name from " . $config["db_name"] . ".roles");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getTypes() {
        global $connection;global $config;

        $statement = $connection -> prepare("select name from " . $config["db_name"] . ".types");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getResources() {
        global $connection;global $config;

        $statement = $connection -> prepare("select name from " . $config["db_name"] . ".resources");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getBuildings() {
        global $connection;global $config;

        $statement = $connection -> prepare("select name from " . $config["db_name"] . ".buildings");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getResourcesRoom($room) {
        global $connection;global $config;

        $statement = $connection -> prepare("select resources.name
                                                from " . $config["db_name"] . ".resource_list
                                                inner join " . $config["db_name"] . ".rooms on rooms.id=resource_list.room_id
                                                inner join " . $config["db_name"] . ".resources on resources.id=resource_list.resource_id
                                                where rooms.name = ?");

        $statement -> execute(array($room));

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getSubjectsStudent($username) {
        global $connection;global $config;

        $statement = $connection -> prepare("select subjects.name, teachers.name as teacher, subjects.year, subjects.subgroup, subjects.degree, subjects.day, subjects.beginning, subjects.duration, subjects.startdate, subjects.enddate, subjects.skipdates, rooms.name as room, buildings.name as building
                                            from " . $config["db_name"] . ".users users
                                            inner join " . $config["db_name"] . ".subjects on (users.year=subjects.year and users.degree=subjects.degree and (users.subgroup=subjects.subgroup or subjects.subgroup is null))
                                            inner join " . $config["db_name"] . ".rooms on rooms.id = subjects.room_id
                                            inner join " . $config["db_name"] . ".users teachers on teachers.id=subjects.teacher_id
                                            inner join " . $config["db_name"] . ".buildings on buildings.id=rooms.building_id
                                            where users.username = ?");
        $statement -> execute(array($username));

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getSubjectsTeacher($username) {
        global $connection;global $config;

        $statement = $connection -> prepare("select subjects.name, users.name as teacher, subjects.year, subjects.subgroup, subjects.degree, subjects.day, subjects.beginning, subjects.duration, subjects.startdate, subjects.enddate, subjects.skipdates, rooms.name as room, buildings.name as building
                                            from " . $config["db_name"] . ".users users
                                            inner join " . $config["db_name"] . ".subjects on (users.id=subjects.teacher_id)
                                            inner join " . $config["db_name"] . ".rooms on rooms.id = subjects.room_id
                                            inner join " . $config["db_name"] . ".buildings on buildings.id=rooms.building_id
                                            where users.username = ?");

        $statement -> execute(array($username));

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getSubjectsRoom($room) {
        global $connection;global $config;

        $statement = $connection -> prepare("select subjects.name, users.name as teacher, subjects.year, subjects.subgroup, subjects.degree, subjects.day, subjects.beginning, subjects.duration, subjects.startdate, subjects.enddate, subjects.skipdates, rooms.name as room, buildings.name as building
                                            from " . $config["db_name"] . ".users users
                                            inner join " . $config["db_name"] . ".subjects on (users.id=subjects.teacher_id)
                                            inner join " . $config["db_name"] . ".rooms on rooms.id = subjects.room_id
                                            inner join " . $config["db_name"] . ".buildings on buildings.id=rooms.building_id
                                            where rooms.name = ?");

        $statement -> execute(array($room));

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getSubjects() {
        global $connection;global $config;

        $statement = $connection -> prepare("select subjects.name, users.name as teacher, subjects.year, subjects.subgroup, subjects.degree, subjects.day, subjects.beginning, subjects.duration, subjects.startdate, subjects.enddate, subjects.skipdates, rooms.name as room
                                            from " . $config["db_name"] . ".users
                                            inner join " . $config["db_name"] . ".subjects on (users.id=subjects.teacher_id)
                                            inner join " . $config["db_name"] . ".rooms on rooms.id = subjects.room_id;");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getRooms() {
        global $connection;global $config;

        $statement = $connection -> prepare("select rooms.name, types.name as type, rooms.capacity, buildings.name as building
                                            from " . $config["db_name"] . ".rooms
                                            inner join " . $config["db_name"] . ".types on (types.id=rooms.type_id)
                                            inner join " . $config["db_name"] . ".buildings on (rooms.building_id = buildings.id);");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getUsers() {
        global $connection;global $config;

        $statement = $connection -> prepare("select users.username, roles.name as role, users.name, users.year, users.subgroup, users.degree
                                            from " . $config["db_name"] . ".users
                                            inner join " . $config["db_name"] . ".roles on users.role_id=roles.id");

        $statement -> execute(array());

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getRoomResource($resource) {
        global $connection;global $config;

        $statement = $connection -> prepare("select rooms.name
                                                from " . $config["db_name"] . ".resource_list
                                                inner join " . $config["db_name"] . ".rooms on rooms.id=resource_list.room_id
                                                inner join " . $config["db_name"] . ".resources on resources.id=resource_list.resource_id
                                                where resources.name = ?");

        $statement -> execute(array($resource));

        $result = $statement -> fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    function getRoomResources($resources) {
        $result = getRoomResource($resources[0]);

        foreach($resources as $resource) {
            $result = @array_intersect_assoc($result, getRoomResource($resource));
        }

        return $result;
    }


    function deleteResource($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".resources
                                                where resources.name = ?");

        $statement -> execute(array($name));
    }

    function deleteBuilding($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".buildings
                                                where buildings.name = ?");

        $statement -> execute(array($name));
    }

    function deleteType($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".types
                                                where types.name = ?");

        $statement -> execute(array($name));
    }

    function deleteRoom($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".rooms
                                                where rooms.name = ?");

        $statement -> execute(array($name));
    }

    function deleteRole($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".roles
                                                where roles.name = ?");

        $statement -> execute(array($name));
    }

    function deleteUser($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".users
                                                where users.name = ?");

        $statement -> execute(array($name));
    }

    function deleteSubject($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".subjects
                                                where subjects.name = ?");

        $statement -> execute(array($name));
    }

    function removeResourceFromRoom($resource, $room) {
        global $connection;global $config;

        $statement = $connection -> prepare("select id from " . $config["db_name"] . ".resources where resources.name=?");
        $statement -> execute(array($resource));
        $reid = $statement -> fetch(PDO::FETCH_ASSOC);

        $statement = $connection -> prepare("select id from " . $config["db_name"] . ".rooms where rooms.name=?");
        $statement -> execute(array($room));
        $roid = $statement -> fetch(PDO::FETCH_ASSOC);

        $statement = $connection -> prepare("delete from " . $config["db_name"] . ".resource_list
                                                where resource_list.resource_id = ? and resource_list.room_id = ?");

        $statement -> execute(array($reid["id"], $roid["id"]));

    }


 ?>
