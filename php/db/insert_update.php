<?php
    include("init.php");

    function insertResource($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".resources (name)
                                                values(?)");
        $statement -> execute(array($name));
    }

    function insertBuilding($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".buildings (name)
                                                values(?)");
        $statement -> execute(array($name));
    }

    function insertType($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".types (name)
                                                values(?)");
        $statement -> execute(array($name));
    }

    function insertRoles($name) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".roles (name)
                                                values(?)");
        $statement -> execute(array($name));
    }

    function insertRoom($name, $typeId, $capacity, $buildingId) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".rooms (name, type_id, capacity, building_id)
                                                values(?,?,?,?)");
        $statement -> execute(array($name, $typeId, $capacity, $buildingId));
    }

    function insertResourceList($resourceId, $roomId) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".resource_list (resource_id, room_id)
                                                values(?,?)");
        $statement -> execute(array($resourceId, $roomId));
    }

    function insertUser($username, $passhash, $roleId, $name, $year, $subgroup, $degree) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".users (username, passhash, role_id, name, year, subgroup, degree)
                                                values(?,?,?,?,?,?,?)");
        $statement -> execute(array($username, $passhash, $roleId, $name, $year, $subgroup, $degree));
    }

    function insertSubject($name, $teacher_id, $year, $subgroup, $degree, $day, $beginning, $duration, $startdate, $enddate, $skipdates, $roomId) {
        global $connection;global $config;

        $statement = $connection -> prepare("insert into " . $config["db_name"] . ".subjects (name, teacher_id, year, subgroup, degree, day, beginning, duration, startdate, enddate, skipdates, room_id)
                                                values(?,?,?,?,?,?,?,?,?,?,?,?)");
        $statement -> execute(array($name, $teacher_id, $year, $subgroup, $degree, $day, $beginning, $duration, $startdate, $enddate, $skipdates, $roomId));
    }

 ?>
