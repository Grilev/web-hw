<?php
    include("init.php");

    function getPassword($username) {
        global $connection;
        global $config;
        
        $statement = $connection -> prepare("select passhash from " . $config["db_name"] . ".users where username = ?;");
        $statement -> execute(array($username));

        $pass = $statement -> fetch();

        return $pass["passhash"];
    }

    function authenticate($username, $password) {
        $hash = getPassword($username);

        return password_verify($password, $hash);
    }

 ?>
