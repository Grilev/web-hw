<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Добави към базата</title>

        <link rel="stylesheet" href="../../css/styles.css">
    </head>
    <body>
        <?php include("navigation.php") ?>

        <div>

            <form class="" action="templates/users.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за потребители">
            </form>

            <form action="handleUsers.php" method="post" enctype="multipart/form-data">
                Добави нови потребители:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/roles.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за роли">
            </form>

            <form action="handleRoles.php" method="post" enctype="multipart/form-data">
                Добави нови роли:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/types.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за типове зали">
            </form>

            <form action="handleTypes.php" method="post" enctype="multipart/form-data">
                Добави нови типове зали:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/buildings.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за сгради">
            </form>

            <form action="handleBuildings.php" method="post" enctype="multipart/form-data">
                Добави нови сгради:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/resources.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за сгради">
            </form>

            <form action="handleResources.php" method="post" enctype="multipart/form-data">
                Добави нови ресурси:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/resourceList.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за добавяне на ресурси към зали">
            </form>

            <form action="handleResourceList.php" method="post" enctype="multipart/form-data">
                Добави нови ресурси към зали:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/rooms.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за зали">
            </form>

            <form action="handleRooms.php" method="post" enctype="multipart/form-data">
                Добави нови зали:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <div>

            <form class="" action="templates/subjects.csv" method="get">
                <input type="submit" name="" value="Изтегли csv template за предмети">
            </form>

            <form action="handleSubjects.php" method="post" enctype="multipart/form-data">
                Добави нови предмети:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input type="submit" name="" value="добави">
            </form>

        </div>

        <form class="" action="logout.php" method="get">
            <input type="submit" name="" value="излез">
        </form>
    </body>
</html>
