<?php

    if (isset($_FILES["fileToUpload"])) {
        $fd = fopen($_FILES["fileToUpload"]["tmp_name"],"r");
        fgetcsv($fd);

        include("../db/insert_update.php");

        while($data = fgetcsv($fd)){
            $roomId = $data[0];
            $resourceId = $data[1];

            insertResourceList($resourceId, $roomId);
        }
    }

    header("Location: home.php");

 ?>
