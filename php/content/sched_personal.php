<?php
    session_start();
    if (!isset($_SESSION["username"])) {
        header("Location: ../../index.html");
    }
    else {
        include("../db/get_delete.php");

        if ($_SESSION["role"] == "student") {
            $result = getSubjectsStudent($_SESSION["username"]);
        }

        else {
            $result = getSubjectsTeacher($_SESSION["username"]);
        }

        echo json_encode($result);
        //return json_encode($result);
    }
 ?>
