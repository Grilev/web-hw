<?php
    session_start();

    if (!isset($_SESSION["username"])) {
        header("Location: ../../index.html");
    }

    $user = $_SESSION["username"];

    include("../db/get_delete.php");

    $role = getRole($user);
?>
<nav>
    <ul>
        <?php if ($user != "admin") {?>
            <li><a href="schedule.php">Моят график</a></li>
            <li><a href="#">Съобщения</a></li>
        <?php } ?>

        <li><a href="search.php">Търсене</a></li>

        <?php if ($role == "teacher") {?>
                <li><a href="#">Направи заявка</a></li>
        <?php  }?>

        <?php if ($user == "admin") {?>
            <li><a href="#">Получени заявки</a></li>
            <li><a href="#">Направи промяна</a></li>
            <li><a href="add.php">Добави</a></li>
        <?php } ?>
    </ul>

</nav>
