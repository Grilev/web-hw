<?php
    session_start();

    if (!isset($_SESSION["username"])) {
        header("Location: ../../index.html");
    }


    else {

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


        include("../db/get_delete.php");



        $name = $_POST["usr"];
        $_SESSION["roomcsv"]=$name;
        $result = getSubjectsRoom($name);

        echo json_encode($result);
        return json_encode($result);
    }
 ?>
