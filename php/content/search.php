<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Търсене</title>

        <link rel="stylesheet" href="../../css/styles.css">
    </head>
    <body>
        <?php include("navigation.php") ?>

        <table id="sched_room">
        </table>

        Въведи име на зала: <input type="text" name="date_r" value="" id="date_room" onchange="postajax_room()">

        <form class="" action="export_room_sched.php" method="get">
            <input type="submit" name="" value="Изтегли csv">
        </form>


        <table id="sched_name">
        </table>

        Въведи име на потребител: <input type="text" name="date_n" value="" id="date_name" onchange="postajax_name()">
        <form class="" action="export_name_sched.php" method="get">
            <input type="submit" name="" value="Изтегли csv">
        </form>


        <form class="" action="logout.php" method="get">
            <input type="submit" name="" value="излез">
        </form>

        <script type="text/javascript" src="../../js/schedule_name.js"></script>
        <script type="text/javascript" src="../../js/schedule_room.js"></script>
    </body>
</html>
