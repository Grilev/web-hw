<?php

    if (isset($_FILES["fileToUpload"])) {
        $fd = fopen($_FILES["fileToUpload"]["tmp_name"],"r");
        fgetcsv($fd);

        include("../db/insert_update.php");

        while($data = fgetcsv($fd)){
            $username = $data[0];
            $passhash = password_hash($data[1], PASSWORD_DEFAULT);
            $roleId = $data[2];
            $name = $data[3];
            $year = $data[4];
            $subgroup = $data[5];
            $degree = $data[6];

            insertUser($username, $passhash, $roleId, $name, $year, $subgroup, $degree);
        }
    }

    header("Location: home.php");

 ?>
