<?php

    if (isset($_FILES["fileToUpload"])) {
        $fd = fopen($_FILES["fileToUpload"]["tmp_name"],"r");
        fgetcsv($fd);

        include("../db/insert_update.php");

        while($data = fgetcsv($fd)){
            $name = $data[0];
            $teacherId = $data[1];
            $year = $data[2];
            $subgroup = $data[3];
            $degree = $data[4];
            $day = $data[5];
            $beginning = $data[6];
            $duration = $data[7];
            $start = $data[8];
            $end = $data[9];
            $skip = $data[10];
            $roomId = $data[11];

            insertSubject($name, $teacherId, $year, $subgroup, $degree, $day, $beginning, $duration, $start, $end, $skip, $roomId);
        }
    }

    header("Location: home.php");

 ?>
