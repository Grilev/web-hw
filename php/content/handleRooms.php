<?php

    if (isset($_FILES["fileToUpload"])) {
        $fd = fopen($_FILES["fileToUpload"]["tmp_name"],"r");
        fgetcsv($fd);

        include("../db/insert_update.php");

        while($data = fgetcsv($fd)){
            $name = $data[0];
            $typeId = $data[1];
            $capacity = $data[2];
            $buildingId = $data[3];

            insertRoom($name, $typeId, $capacity, $buildingId);
        }
    }

    header("Location: home.php");

 ?>
