<?php
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename=schedule.csv;');

    $output = fopen("php://output", "w");

    include("../db/get_delete.php");

    session_start();

    $results = getSubjectsRoom($_SESSION["roomcsv"]);


    if (count($results) > 0) {
        foreach ($results[0] as $key => $val) {
            $header[$key] = $key;
        }
    }

    fputcsv($output, $header);

    foreach ($results as $res) {
        fputcsv($output, $res);
    }
 ?>
