<?php
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename=schedule.csv;');

    $output = fopen("php://output", "w");

    include("../db/get_delete.php");

    session_start();


    $name=$_SESSION["namecsv"];
    $info = getUserInfoName($name);

    if ($info["role"] == "student") {
        $results = getSubjectsStudent($info["username"]);
    }

    else {
        $results = getSubjectsTeacher($info["username"]);
    }

    if (count($results) > 0) {
        foreach ($results[0] as $key => $val) {
            $header[$key] = $key;
        }
    }

    fputcsv($output, $header);

    foreach ($results as $res) {
        fputcsv($output, $res);
    }
 ?>
