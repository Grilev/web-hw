<?php

include("../db/authenticator.php");

if ($_POST["username"] == "admin" && $_POST["password"] == "admin") {
    session_start();
    $_SESSION["username"] = $_POST["username"];
    $_SESSION["name"] = "Admin";

    header("Location: home.php");
}

else if (authenticate($_POST["username"], $_POST["password"])) {
    session_start();
    $_SESSION["username"] = $_POST["username"];

    include("../db/get_delete.php");

    $info = getUserInfo($_POST["username"]);
    $_SESSION["name"] = $info["name"];
    $_SESSION["role"] = $info["role"];

    header("Location: home.php");
}

else {
    header("Location: ../../index.html");
}

?>
